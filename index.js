// load express and path
var express = require("express");
var path = require("path");

// create an instance of express app
var app = express();

var argv = process.argv;
var env = process.env;

// configure port
var setPort = parseInt(argv[2]) || parseInt(env.APP_PORT) || 3000;
console.log("setting port to: %s of type %s", setPort, typeof setPort);
app.set("port", setPort);

// process.env

// for (var  i = 0; i < argv.length; i++)
// console.log("argv [%d] is %s", i, argv[i]);

// app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, "public")));
// app.use("test",express.static(path.join(__dirname, "public")));

var imgArray = ["1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg"]

app.use(function(rep, resp){
  resp.status('404');
  resp.type("text/html");
  var num = Math.floor(Math.random() * imgArray.length);
  resp.send("<img src='/images/" + imgArray[num] + "', alt='logo'>");
})

// start web server on port
app.listen(setPort, function() {
  console.log("Web server started on port: %d", setPort);
});